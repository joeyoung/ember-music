/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp();

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

// install bootstrap with ember install:bower bootstrap
// add the following to get the bootstrap css in the dist folder
app.import('bower_components/bootstrap/dist/css/bootstrap.css');

// also need to get the map (only in development)
if (app.env === 'development') {
    app.import('bower_components/bootstrap/dist/css/bootstrap.css.map', {
      destDir: 'assets'
    });
}

// install font-awesome with ember install:bower font-awesome
app.import('bower_components/font-awesome/css/font-awesome.css');

// import the fonts to be placed in the dist folder
app.import('bower_components/font-awesome/fonts/fontawesome-webfont.ttf', {
    destDir: 'fonts'
});
app.import('bower_components/font-awesome/fonts/fontawesome-webfont.woff', {
    destDir: 'fonts'
});
app.import('bower_components/font-awesome/fonts/fontawesome-webfont.woff2', {
    destDir: 'fonts'
});

module.exports = app.toTree();
